package com.svspatna.service.student;

import com.svspatna.bean.Student;

public interface StudentService {
	
	public int addStudent(Student s);
	public int removeStudent(String id);
	public void showStudentDetails(String id);
	
	public int feePayment(String id);
	public void printPaymentHistory(String id);
	
	public boolean promoteClass(String id);
	public boolean marksEntry(String id);
	public void printProgressReport(String id);
	
}
