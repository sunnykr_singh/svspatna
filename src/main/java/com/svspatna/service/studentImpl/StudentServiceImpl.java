package com.svspatna.service.studentImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.InputMismatchException;
import java.util.Scanner;

import com.svspatna.bean.Student;
import com.svspatna.bean.Student.Fee;
import com.svspatna.bean.StudentProgressReport;
import com.svspatna.dao.StudentDao;
import com.svspatna.factory.SchoolFactory;
import com.svspatna.payment.PaymentModes;
import com.svspatna.service.student.StudentService;

public class StudentServiceImpl implements StudentService {

	static Scanner read = new Scanner(System.in);

	@Override
	public int addStudent(Student sbean) {
		String id = generateID();
		sbean.setId(id);
		return ((StudentDao) SchoolFactory.getInstance("StudentDao")).addStudent(sbean);
	}

	private String generateID() {
		boolean unique = false;
		String id = "svs";
		while (!unique) {
			id = id + (int) (Math.floor(Math.random() * 9000000L) + 1000000L);
			if (findStudent(id) == null) {
				unique = true;
			}
		}
		return id;
	}

	@Override
	public int removeStudent(String id) {
		Student s = findStudent(id);
		if (s != null) {
			boolean noDues = true;
			Student.Fee sfee = ((StudentDao) SchoolFactory.getInstance("StudentDao")).getMonthlyPaybleAmmount(id);
			if(sfee.getDues() > 0) {
				noDues = false;
				System.out.println("Dues Ammount to be Paid : " + sfee.getDues());
				System.out.print("Pay Now [Y/N] ? ");
				if(read.nextLine().equalsIgnoreCase("Y"))
					noDues = true;
			}
			if(noDues)
				return ((StudentDao) SchoolFactory.getInstance("StudentDao")).removeStudent(id);
		}
		else
			System.out.println("No Student found");
		return 0;
	}

	@Override
	public int feePayment(String id) {
		Student sbean = findStudent(id);
		if (sbean != null) {
			Student.Fee sfee = ((StudentDao) SchoolFactory.getInstance("StudentDao")).getMonthlyPaybleAmmount(id);
			double paybalAmmount = sfee.getMonthlyFee() + sfee.getTransportationCost() + sfee.getDues();
			System.out.println("Ammount to be paid : " + paybalAmmount);
			System.out.print("Enter Amount\t:");
			sfee.setPaidAmmount(read.nextDouble());
			read.nextLine();
			sfee.setDues(paybalAmmount - sfee.getPaidAmmount());
			System.out.println("Select Payment Mode :\n"
							+ "\t1. Cash\t\t2. Cheque\n"
							+ "\t3. Card\t\t4. UPI");
			sfee.setPaymentMode(PaymentModes.getPaymentMode(read.nextInt()).toString());
			read.nextLine();
			if(sfee.getPaymentMode().equals(PaymentModes.CASH.getReasonPhrase()))
				System.out.print("Enter Recipt Number\t: ");
			else
				System.out.print("Enter Transaction Reference : ");
			sfee.setTransactionReference(read.nextLine());
			sfee.setPayDate(new Date());
			return ((StudentDao) SchoolFactory.getInstance("StudentDao")).feePayment(sfee, id);
		}
		System.out.println("No Student found");
		return 0;
	}
	
	@Override
	public void printPaymentHistory(String id) {
		ArrayList<Fee> allFeePayments = 
				new ArrayList<>((((StudentDao) SchoolFactory.getInstance("StudentDao")).getPaymentHistory(id)));
		SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
		System.out.println("Ammount Paid\tPay Date\tDues Ammount\tPayment Mode\tReference");
		System.out.println("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -");
		for(Fee f : allFeePayments) {
			System.out.println(f.getPaidAmmount() + "\t\t" + sdf.format(f.getPayDate()) + "\t" + f.getDues() +
					"\t\t" + f.getPaymentMode() + "\t\t" + f.getTransactionReference());
		}
	}

	@Override
	public boolean promoteClass(String id) {
		Student s = findStudent(id);
		if (s != null) {
			ArrayList<StudentProgressReport> prep = ((StudentDao) SchoolFactory.getInstance("StudentDao"))
					.getStudentProgressReport(id);
			try {
				StudentProgressReport pr = prep.get(prep.size() - 1);// get report of the last exam attended by the
																		// Student
				if (pr.getExamType().equalsIgnoreCase("Annual")) {
					if (pr.isPass()) {
						System.out.print("Enter Class Section : ");
						String newClassSection = read.nextLine();
						int i = ((StudentDao) SchoolFactory.getInstance("StudentDao")).promoteStudent(id,
								newClassSection);
						if (i > 0)
							System.out.println(s.getName() + " is promoted to " + newClassSection);
						return true;
					} else {
						System.out.println(s.getName() + " Faild in last Exam");
					}
				} else {
					System.out.println(s.getName() + " Has not taken Annual exam");
				}
			} catch (IndexOutOfBoundsException e) {
				System.out.println("The progress report of " + s.getName() + " is empty");
			}
		}
		return false;
	}

	@Override
	public boolean marksEntry(String id) {
		Student s = findStudent(id);
		if (s != null) {
			StudentProgressReport progRep = (StudentProgressReport) SchoolFactory.getInstance("StudentProgressReport");
			System.out.print("Enter Exam Type\t\t: ");
			progRep.setExamType(read.nextLine());
			System.out.print("Enter Date of Exam\t: ");
			try {
				progRep.setDate(new SimpleDateFormat("dd-MM-yyyy").parse(read.nextLine()));
			} catch (ParseException e) {
				System.out.println(e.getMessage());
			}
			int totalMarks = 0;
			Dictionary<String, Integer> subMarks = new Hashtable<String, Integer>();
			for (int i = 0; i < 6; i++) {
				try {
					System.out.print("\t" + StudentProgressReport.Subject.getSubject(i) + "   \t: ");
					int marks = read.nextInt();
					read.nextLine();
					subMarks.put(StudentProgressReport.Subject.getSubject(i).toString(), marks);
					totalMarks += marks;
				} catch (InputMismatchException e) {
					System.out.println("Opps..! Incorrect Data Entry\ntry Again..");
					i--;
					read.nextLine();
					continue;
				}
			}
			progRep.setMarks(subMarks);
			progRep.setPass((totalMarks / subMarks.size() >= 35) ? true : false);
			((StudentDao) SchoolFactory.getInstance("StudentDao")).insertProgressReport(progRep, id);
		}
		return false;
	}

	@Override
	public void printProgressReport(String id) {
		ArrayList<StudentProgressReport> pRepo = ((StudentDao) SchoolFactory.getInstance("StudentDao"))
					.getStudentProgressReport(id);
		if(pRepo != null) {
			for (StudentProgressReport p : pRepo) {
				System.out.println("------------------------------------");
				System.out.println(p.getExamType() + " Examination " + p.getDate());
				System.out.println("------------------------------------");
				System.out.println("Subject\t\t\tMarks");
				System.out.println("- - - - - - - - - - - - - - - - - -");
				Enumeration<String> sub = p.getMarks().keys();
				Enumeration<Integer> marks = p.getMarks().elements();
				while (sub.hasMoreElements()) {
					System.out.println(sub.nextElement() + "   \t\t" + marks.nextElement());
				}
				if (p.isPass())
					System.out.println("Remark : Pass");
				else
					System.out.println("Remark : Fail");
			}
		}else {
			System.out.println("No Data Found");
		}
	}

	@Override
	public void showStudentDetails(String id) {
		Student s = findStudent(id);
		if (s != null) {
			Fee sfee = (Fee) ((StudentDao) SchoolFactory.getInstance("StudentDao")).getFeeDetails(id);
			System.out.println("------------------------------------");
			System.out.println(s.toString());
			if (s.isSchoolTransport()) {
				System.out.println("School Transport: Yes");
			} else {
				System.out.println("School Transport: No");
			}
			System.out.println("Fee Details ------------------------");
			System.out.println(sfee.toString());
		} else {
			System.out.println("No Student found");
		}
	}

	private Student findStudent(String id) {
		return ((StudentDao) SchoolFactory.getInstance("StudentDao")).getStudentData(id);
	}

}
