package com.svspatna.service.employee;

import com.svspatna.bean.Employee;

public interface EmployeeService {
	
	public boolean addEmployee(Employee emp);
	public void makeAttandance(String ecode);
	public void paySalary(String ecode);
	public void removeEmployee(String ecode);
	
}
