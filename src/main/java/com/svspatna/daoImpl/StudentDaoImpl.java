package com.svspatna.daoImpl;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.sql.Connection;
import java.sql.Date;

import com.svspatna.bean.Address;
import com.svspatna.bean.Student;
import com.svspatna.bean.Student.Fee;
import com.svspatna.bean.StudentProgressReport;
import com.svspatna.dao.StudentDao;
import com.svspatna.factory.SchoolFactory;
import com.svspatna.util.DaoConnection;

public class StudentDaoImpl implements StudentDao {
	
	static Connection con;
	static ResultSet rs;
	static PreparedStatement pst;
	
	@Override
	public int addStudent(Student sbean) {
		con = DaoConnection.getConnection();
		final String ADD_STUDENT = "INSERT INTO svspatnadb.students VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
		final String FEE_DETAILS = "INSERT INTO svspatnadb.student_fee values(?,?,?,?,?,?,?,?)";
		try {
			pst = con.prepareStatement(ADD_STUDENT);
			pst.setString(1, sbean.getId());
			pst.setString(2, sbean.getName());
			pst.setString(3, sbean.getClassSection());
			pst.setString(4, sbean.getFatherName());
			pst.setString(5, sbean.getMotherName());
			pst.setString(6, sbean.getAddress().getState());
			pst.setString(7, sbean.getAddress().getCity());
			pst.setString(8, sbean.getAddress().getArea());
			pst.setInt(9, sbean.getAddress().getZipCode());
			pst.setString(10, sbean.getAddress().getHouseNumber());
			pst.setBoolean(11, sbean.isSchoolTransport());
			pst.setString(12, sbean.getContact());
			pst.setDate(13, new Date(sbean.getDob().getTime()));
			pst.executeUpdate();
			
			pst = con.prepareStatement(FEE_DETAILS);
			pst.setDouble(1, sbean.getFee().get(0).getAddmissionFee());
			pst.setDouble(2, sbean.getFee().get(0).getMonthlyFee());
			pst.setDouble(3, sbean.getFee().get(0).getUniformCost());
			pst.setDouble(4, sbean.getFee().get(0).getBooksCost());
			pst.setDouble(5, sbean.getFee().get(0).getExtracurricularCharges());
			pst.setDouble(6, sbean.getFee().get(0).getTransportationCost());
			pst.setString(7, sbean.getId());
			pst.setDouble(8, sbean.getFee().get(0).getDues());
			return pst.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return 0;
	}

	@Override
	public int removeStudent(String id) {
		con = DaoConnection.getConnection();
		final String DELETE_STUDENT_DATA = "DELETE FROM svspatnadb.students WHERE id = ?";
		final String DELETE_STUDENT_FEE_DEAILS = "DELETE FROM svspatnadb.student_fee WHERE id = ?";
		final String DELETE_STUDENT_FEE_RECORDS = "DELETE FROM svspatnadb.fee_payment_records WHERE id = ?";
		final String DELETE_STUDENT_PROGRESS_REPORT = "DELETE svspatnadb.FROM fee_payment_records WHERE id = ?";
		try {
			pst = con.prepareStatement(DELETE_STUDENT_FEE_DEAILS);
			pst.setString(1, id);
			pst.executeUpdate();
			pst = con.prepareStatement(DELETE_STUDENT_FEE_RECORDS);
			pst.setString(1, id);
			pst.executeUpdate();
			pst = con.prepareStatement(DELETE_STUDENT_PROGRESS_REPORT);
			pst.setString(1, id);
			pst.executeUpdate();
			pst = con.prepareStatement(DELETE_STUDENT_DATA);
			pst.setString(1, id);
			return pst.executeUpdate();
		}catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}

	@Override
	public Student getStudentData(String id) {
		con = DaoConnection.getConnection();
		final String GET_STUDENT_DATA = "SELECT name, class, father, mother, state, city, area, zipcode, house, "
				+ "transport, contact, dob FROM svspatnadb.students WHERE id = ?";
		try {
			pst = con.prepareStatement(GET_STUDENT_DATA);
			pst.setString(1, id);
			rs = pst.executeQuery();
			if (rs.next()) {
				
				Student sbean = (Student)SchoolFactory.getInstance("Student");
				Address addr = (Address) SchoolFactory.getInstance("Address");
				sbean.setName(rs.getString(1));
				sbean.setClassSection(rs.getString(2));
				sbean.setFatherName(rs.getString(3));
				sbean.setMotherName(rs.getString(4));
				sbean.setSchoolTransport(rs.getBoolean(10));
				sbean.setContact(rs.getString(11));
				sbean.setDob(new java.util.Date(rs.getDate(12).getTime()));
				addr.setState(rs.getString(5));
				addr.setCity(rs.getString(6));
				addr.setArea(rs.getString(7));
				addr.setZipCode(rs.getInt(8));
				addr.setHouseNumber(rs.getString(9));
				sbean.setAddress(addr);
				
				return sbean;
			}
			
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		
		return null;
	}

	@Override
	public int feePayment(Student.Fee fee, String id) {
		con = DaoConnection.getConnection();
		final String PAY_FEE = "INSERT INTO svspatnadb.fee_payment_records (paid_amt, date, dues, pay_mode, trans_no,"
							+ " id) VALUES (?,?,?,?,?,?)";
		final String UPDATE_DUES = "UPDATE svspatnadb.student_fee SET dues = ? WHERE id = ?";
		try {
			pst = con.prepareStatement(PAY_FEE);
			pst.setDouble(1, fee.getPaidAmmount());
			pst.setDate(2, new Date(fee.getPayDate().getTime()));
			pst.setDouble(3, fee.getDues());
			pst.setString(4, fee.getPaymentMode());
			pst.setString(5, fee.getTransactionReference());
			pst.setString(6, id);
			pst.executeUpdate();
			
			pst = con.prepareStatement(UPDATE_DUES);
			pst.setDouble(1, fee.getDues());
			pst.setString(2, id);
			pst.executeUpdate();
			return 1;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return 0;
	}
	
	@Override
	public ArrayList<Fee> getPaymentHistory(String id) {
		con = DaoConnection.getConnection();
		final String GET_PAYMENT_HISTORY = "SELECT paid_amt, date, "
				+ "dues, pay_mode, trans_no FROM svspatnadb.fee_payment_records WHERE id = ?";
		ArrayList<Fee> allFeePayments = new ArrayList<>();
		try {
			pst = con.prepareStatement(GET_PAYMENT_HISTORY);
			pst.setString(1, id);
			rs = pst.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					Fee sfee = (Fee) SchoolFactory.getInstance("Fee");
					sfee.setPaidAmmount(rs.getDouble(1));
					sfee.setPayDate(new java.util.Date(rs.getDate(2).getTime()));
					sfee.setDues(rs.getDouble(3));
					sfee.setPaymentMode(rs.getString(4));
					sfee.setTransactionReference(rs.getString(5));
					allFeePayments.add(sfee);
				}
				return allFeePayments;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	@Override
	public Fee getFeeDetails(String id) {
		con = DaoConnection.getConnection();
		final String GET_FEE_DETAILS = "SELECT addmission_fee, monthly_fee, uniform_cost, book_cost, "
				+ "extra_charges, transport, dues FROM svspatnadb.student_fee WHERE id = ?";
		try {
			pst = con.prepareStatement(GET_FEE_DETAILS);
			pst.setString(1, id);
			rs = pst.executeQuery();
			if(rs.next()) {
				Fee sfee = (Fee) SchoolFactory.getInstance("Fee");
				sfee.setAddmissionFee(rs.getDouble(1));
				sfee.setMonthlyFee(rs.getDouble(2));
				sfee.setUniformCost(rs.getDouble(3));
				sfee.setBooksCost(rs.getDouble(4));
				sfee.setExtracurricularCharges(rs.getDouble(5));
				sfee.setTransportationCost(rs.getDouble(6));
				sfee.setDues(rs.getDouble(7));
				return sfee;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
	
	@Override
	public Fee getMonthlyPaybleAmmount(String id) {
		con = DaoConnection.getConnection();
		final String GET_MONTHLY_FEE = "SELECT monthly_fee, transport, dues FROM svspatnadb.student_fee WHERE id = ?";
		try {
			pst = con.prepareStatement(GET_MONTHLY_FEE);
			pst.setString(1, id);
			rs = pst.executeQuery();
			Fee sfee = (Fee) SchoolFactory.getInstance("Fee");
			if (rs != null) {
				while (rs.next()) {
					sfee.setMonthlyFee(rs.getDouble(1));
					sfee.setTransportationCost(rs.getDouble(2));
					sfee.setDues(rs.getDouble(3));
				}
				return sfee;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	@Override
	public int insertProgressReport(StudentProgressReport pReport, String id) {
		con = DaoConnection.getConnection();
		final String ADD_PROGRESS_REPORT = "INSERT INTO svspatnadb.progress_report VALUES(?,?,?,?,?,?,?,?,?,?)";
		try {
			pst = con.prepareStatement(ADD_PROGRESS_REPORT);
			pst.setString(1, pReport.getExamType());
			pst.setDate(2, new Date(pReport.getDate().getTime()));
			pst.setBoolean(3, pReport.isPass());
			pst.setInt(4, pReport.getMarks().get("ENGLISH"));
			pst.setInt(5, pReport.getMarks().get("HINDI"));
			pst.setInt(6, pReport.getMarks().get("SCIENCE"));
			pst.setInt(7, pReport.getMarks().get("SOCIAL_SC"));
			pst.setInt(8, pReport.getMarks().get("MATHEMATICS"));
			pst.setInt(9, pReport.getMarks().get("SANSKTRIT"));
			pst.setString(10, id);
			return pst.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}	
		return 0;
	}

	@Override
	public ArrayList<StudentProgressReport> getStudentProgressReport(String id) {
		con = DaoConnection.getConnection();
		final String GET_PROGRESS_REPORT = "SELECT exam_type, date, is_pass, english, hindi, science, social_science, "
				+ "mathematics, sanskrit FROM svspatnadb.progress_report WHERE id = ?";
		try {
			pst = con.prepareStatement(GET_PROGRESS_REPORT);
			pst.setString(1, id);
			rs = pst.executeQuery();
			if(rs!=null) {
			ArrayList<StudentProgressReport> reports= new ArrayList<>();
			while (rs.next()) {
				StudentProgressReport pReport = new StudentProgressReport();
				pReport.setExamType(rs.getString(1));
				pReport.setDate(rs.getDate(2));
				pReport.setPass(rs.getBoolean(3));
				Dictionary<String, Integer> subMarks = new Hashtable<String, Integer>();
				subMarks.put("ENGLISH", rs.getInt(4));
				subMarks.put("HINDI", rs.getInt(5));
				subMarks.put("SCIENCE", rs.getInt(6));
				subMarks.put("SOCIAL_SC", rs.getInt(7));
				subMarks.put("MATHEMATICS", rs.getInt(8));
				subMarks.put("SANSKRIT", rs.getInt(9));
				pReport.setMarks(subMarks);
				reports.add(pReport);
			}
			return reports;
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}	
		return null;
	}

	@Override
	public int promoteStudent(String id, String newClassSection) {
		con = DaoConnection.getConnection();
		final String PROMOTE_STUDENT = "UPDATE svspatnadb.students set class = ? WHERE id = ?";
		try {
			pst = con.prepareStatement(PROMOTE_STUDENT);
			pst.setString(1, newClassSection);
			pst.setString(2, id);
			return pst.executeUpdate();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}	
		return 0;
	}

}
