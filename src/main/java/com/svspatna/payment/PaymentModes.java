package com.svspatna.payment;

public enum PaymentModes {
	CASH("CASH"), CHEQUE("CHEQUE"), CARD("CARD"), UPI("UPI");

	private final String reasonPhrase;

	PaymentModes(String reasonPhrase) {
		this.reasonPhrase = reasonPhrase;
	}

	public String getReasonPhrase() {
		return reasonPhrase;
	}
	
	private static PaymentModes[] paymentModes = PaymentModes.values();

	public static PaymentModes getPaymentMode(int i) {
		return paymentModes[i-1];
	}

}
