package com.svspatna.payment;

public interface PaymentMode {
	public int cashPayment(String reciptNo);
	public int chequePayment(String chequeNo);
	public int cardPayment(String transactionRef);
	public int upiPayment(String transactionRef);
}
