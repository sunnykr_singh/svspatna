package com.svspatna;

import java.util.Scanner;
import com.svspatna.admin.StudentAdministration;
import com.svspatna.adminImpl.StudentAdministrationImpl;

public class SVSPatnaMain {
	
	static Scanner read = new Scanner(System.in);

	public static void main(String[] args) {
		StudentAdministration sadmin = new StudentAdministrationImpl();
		boolean quit = false;
		while (!quit) {
			System.out.println("1. Student Administration\n2. Employee Administration");
			int op = read.nextInt();
			read.nextLine();
			switch(op) {
			case 1:
				sadmin.mainMenu();
				break;
			case 2:
				System.out.println("Employee Management System is currently Unavailable");
				break;
			default :
				quit = true;	
			}
		}
	}
}



