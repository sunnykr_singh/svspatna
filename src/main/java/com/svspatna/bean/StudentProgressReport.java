package com.svspatna.bean;

import java.util.Date;
import java.util.Dictionary;

public class StudentProgressReport {
	private String examType;
	private Date date;
	private Dictionary<String, Integer> marks;
	private boolean pass;
		
	public enum Subject {
		ENGLISH, HINDI, SCIENCE, SOCIAL_SC, SANSKTRIT, MATHEMATICS;

		private static Subject[] subjects = Subject.values();

		public static Subject getSubject(int i) {
			return subjects[i];
		}
	}
	
	public StudentProgressReport() {
		
	}

	public String getExamType() {
		return examType;
	}
	
	public Dictionary<String, Integer> getMarks() {
		return marks;
	}
	
	public boolean isPass() {
		return pass;
	}
	
	public void setExamType(String examType) {
		this.examType = examType;
	}

	public void setMarks(Dictionary<String, Integer> marks) {
		this.marks = marks;
	}
	
	public void setPass(boolean pass) {
		this.pass = pass;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}
	
}
