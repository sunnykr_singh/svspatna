package com.svspatna.bean;

import java.time.LocalDate;
import java.time.LocalTime;

public class EmployeeAttandence {
	
	private LocalDate date;
	private LocalTime arrivalTime;
	private LocalTime departureTime;
	
	public EmployeeAttandence(LocalDate date) {
		this.setDate(date);
	}
	
	public LocalDate getDate() {
		return date;
	}
	
	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalTime getDepartureTime() {
		return departureTime;
	}

	public void setDepartureTime(LocalTime departureTime) {
		this.departureTime = departureTime;
	}

	public LocalTime getArrivalTime() {
		return arrivalTime;
	}

	public void setArrivalTime(LocalTime arrivalTime) {
		this.arrivalTime = arrivalTime;
	}
}
