package com.svspatna.bean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class Student {
	
	private String name;
	private String classSection;
	private String fatherName;
	private Date dob;
	private String contact;
	private String motherName;
	private Address address;
	private boolean schoolTransport;
	private ArrayList<Fee> fee;
	private String id;
	private ArrayList<StudentProgressReport> progressReport;
	
	public Student(){
		this.fee = new ArrayList<Fee>();
		this.progressReport = new ArrayList<StudentProgressReport>();
	}
	
	public class Fee {
		
		private double addmissionFee;
		private double monthlyFee;
		private double paidAmmount;
		private double dues;
		private Date payDate;
		private double uniformCost;
		private double booksCost;
		private double extracurricularCharges;
		private double transportationCost;
		private String transactionReference;
		private String paymentMode;
		
		public double getPaidAmmount() {
			return paidAmmount;
		}

		public double getDues() {
			return dues;
		}

		public void setPaidAmmount(double paidAmmount) {
			this.paidAmmount = paidAmmount;
		}

		public void setDues(double dues) {
			this.dues = dues;
		}
		
		public String getPaymentMode() {
			return paymentMode;
		}

		public String getTransactionReference() {
			return transactionReference;
		}

		public void setPaymentMode(String paymentMode) {
			this.paymentMode = paymentMode;
		}

		public void setTransactionReference(String transactionReference) {
			this.transactionReference = transactionReference;
		}

		public double getAddmissionFee() {
			return addmissionFee;
		}
		
		public double getMonthlyFee() {
			return monthlyFee;
		}
		
		public double getUniformCost() {
			return uniformCost;
		}
		
		public double getBooksCost() {
			return booksCost;
		}
		
		public double getExtracurricularCharges() {
			return extracurricularCharges;
		}
		
		public double getTransportationCost() {
			return transportationCost;
		}
		
		public Date getPayDate() {
			return payDate;
		}

		public void setAddmissionFee(double addmissionFee) {
			this.addmissionFee = addmissionFee;
		}

		public void setMonthlyFee(double monthlyFee) {
			this.monthlyFee = monthlyFee;
		}

		public void setPayDate(Date payDate) {
			this.payDate = payDate;
		}

		public void setUniformCost(double uniformCost) {
			this.uniformCost = uniformCost;
		}

		public void setBooksCost(double booksCost) {
			this.booksCost = booksCost;
		}

		public void setExtracurricularCharges(double extracurricularCharges) {
			this.extracurricularCharges = extracurricularCharges;
		}

		public void setTransportationCost(double transportationCost) {
			this.transportationCost = transportationCost;
		}
		
		@Override
		public String toString() {
			return "Addmission Fee\t\t: " + addmissionFee + "\nBooks Cost\t\t: " + booksCost 
					+ "\nExtracurricular Charges : " + extracurricularCharges + "\nMonthly Fee\t\t: " + monthlyFee
					+ "\nTransportation Cost\t: " + transportationCost + "\nUniform Cost\t\t: " + uniformCost
					+ "\nDues\t\t\t: " + dues;
		}

	}
	
	public Date getDob() {
		return dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}
	
	public String getId() {
		return id;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}
	
	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public void setMotherName(String motherName) {
		this.motherName = motherName;
	}

	public void setSchoolTransport(boolean schoolTransport) {
		this.schoolTransport = schoolTransport;
	}

	public void setFee(ArrayList<Fee> fee) {
		this.fee = fee;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setProgressReport(ArrayList<StudentProgressReport> progressReport) {
		this.progressReport = progressReport;
	}
	
	public String getName() {
		return name;
	}
	
	public String getClassSection() {
		return classSection;
	}
	
	public String getFatherName() {
		return fatherName;
	}
	
	public String getMotherName() {
		return motherName;
	}
	
	public String getAddressString() {
		return address.toString();
	}
	
	public Address getAddress() {
		return address;
	}
	
	public boolean isSchoolTransport() {
		return schoolTransport;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setClassSection(String classSection) {
		this.classSection = classSection;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}

	public ArrayList<Fee> getFee() {
		return fee;
	}

	public void setFee(Fee fee) {
		this.fee.add(fee);
	}
	
	public String getFeeStatus(Student s) {
		if(this.fee != null) {
			return "Paid";
		}else {
			return "Not Paid";
		}
	}

	public ArrayList<StudentProgressReport> getProgressReport() {
		return progressReport;
	}
	
	public void addProgressReport(StudentProgressReport progRep) {
		this.progressReport.add(progRep);
	}

	@Override
	public String toString() {
		return "Name\t\t: " + name + "\nClass\t\t: " + classSection
				+ "\nFather's Name\t: " + fatherName + "\nContact Number\t: " + contact
				+ "\nMother's Name\t: " + motherName + "\nAddress\t\t: " + address
				+ "\nDate of Birth\t: " + new SimpleDateFormat("dd-MM-yyyy").format(dob);
	}
	
}
