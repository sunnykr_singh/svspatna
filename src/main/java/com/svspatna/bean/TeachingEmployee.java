package com.svspatna.bean;

import java.time.LocalDate;
import java.util.ArrayList;

public class TeachingEmployee extends Employee {
	
	private static final String EMP_TYPE = "Teacher";
	private String subject;
	private String wing;
	private ArrayList<String> classSections;
	public TeachingEmployee(String ename, double salary, LocalDate dob, LocalDate hireDate, String subject, String wing) {
		super(ename, salary, dob, hireDate);
		this.setSubject(subject);
		this.setWing(wing);
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getWing() {
		return wing;
	}

	public void setWing(String wing) {
		this.wing = wing;
	}

	public ArrayList<String> getClassSections() {
		return classSections;
	}

	public void assignClasses(ArrayList<String> classSections) {
		this.classSections = classSections;
	}

	public static String getEmpType() {
		return EMP_TYPE;
	}

}
