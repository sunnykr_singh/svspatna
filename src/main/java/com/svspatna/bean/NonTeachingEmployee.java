package com.svspatna.bean;

import java.time.LocalDate;

public class NonTeachingEmployee extends Employee{

	private static final String EMP_TYPE = "NonTeaching";
	private String jobRole;
	
	public NonTeachingEmployee(String ename, double salary, LocalDate dob, LocalDate hireDate, String jobRole) {
		super(ename, salary, dob, hireDate);
		this.setJobRole(jobRole);
	}

	public String getJobRole() {
		return jobRole;
	}

	public void setJobRole(String jobRole) {
		this.jobRole = jobRole;
	}

	public static String getEmpType() {
		return EMP_TYPE;
	}
}
