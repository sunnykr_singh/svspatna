package com.svspatna.bean;

import java.time.LocalDate;
import java.util.LinkedList;
import java.time.LocalTime;

public class Employee {
	private String ecode;
	private String ename;
	private double salary;
	private LocalDate dob;
	private LocalDate hireDate;
	private LinkedList<EmployeeAttandence> attandence;

	public Employee(String ename, double salary, LocalDate dob, LocalDate hireDate) {
		this.ename = ename;
		this.salary = salary;
		this.dob = dob;
		this.hireDate = hireDate;
	}

	public void signIn() {
		LocalDate date = LocalDate.now();
		LocalTime time = LocalTime.now();
		EmployeeAttandence today = new EmployeeAttandence(date);
		today.setArrivalTime(time);
	}

	public void signOut() {
		LinkedList<EmployeeAttandence> attandence = this.getAttandence();
		EmployeeAttandence today = attandence.getLast();
		LocalTime time = LocalTime.now();
		today.setDepartureTime(time);
	}

	public String getEcode() {
		return ecode;
	}

	public String getEname() {
		return ename;
	}

	public double getSalary() {
		return salary;
	}

	public LocalDate getDob() {
		return dob;
	}

	public LocalDate getJoinDate() {
		return hireDate;
	}

	public void setEcode(String ecode) {
		this.ecode = ecode;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public void setDob(LocalDate dob) {
		this.dob = dob;
	}

	public void setJoinDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}

	public LinkedList<EmployeeAttandence> getAttandence() {
		return attandence;
	}

	public void setAttandence(LinkedList<EmployeeAttandence> attandence) {
		this.attandence = attandence;
	}

}
