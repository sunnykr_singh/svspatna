package com.svspatna.bean;

public class Address {
	private String state;
	private String city;
	private String area;
	private int zipCode;
	private String houseNumber;
	public String getState() {
		return state;
	}
	public String getCity() {
		return city;
	}
	public String getArea() {
		return area;
	}
	public int getZipCode() {
		return zipCode;
	}
	public String getHouseNumber() {
		return houseNumber;
	}
	public void setState(String state) {
		this.state = state;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public void setArea(String area) {
		this.area = area;
	}
	public void setZipCode(int zipCode) {
		this.zipCode = zipCode;
	}
	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}
	@Override
	public String toString() {
		return area + "," + houseNumber + "\n\t\t  " + city + "," + state + "," + zipCode;
	}
	
	
}
