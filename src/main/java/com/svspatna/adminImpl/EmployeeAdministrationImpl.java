package com.svspatna.adminImpl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

import com.svspatna.admin.EmployeeAdministration;
import com.svspatna.bean.NonTeachingEmployee;
import com.svspatna.bean.TeachingEmployee;
import com.svspatna.service.employee.EmployeeService;
import com.svspatna.service.employeeImpl.EmployeeServiceImpl;

public class EmployeeAdministrationImpl implements EmployeeAdministration{
	private static Scanner read = new Scanner(System.in);
	private static EmployeeService admin = new EmployeeServiceImpl();
	
	public static void mainMenu() {
		boolean quit = false;
		while (!quit) {
			System.out.println(
					"1. Add New Employee\n" + 
					"2. Pay Salary\n" + 
					"3. Make Attandence\n" + 
					"4. Remove Employee\n" +
					"9. Exit"
					);
			int option = read.nextInt();
			read.nextLine();
			switch (option) {
			case 1:
				boolean doneAdding = false;
				while(!doneAdding) {
					doneAdding = addEmployee(admin);
				}
				break;
			case 2:
				break;
			case 3:
				break;
			case 4:
				break;
			default:
				quit = true;
			}
		}
	}

	private static boolean addEmployee(EmployeeService admin) {
		System.out.println("1. Teaching Employee\n2. Non-Teaching Employee");
		int etype = read.nextInt();
		read.nextLine();

		System.out.print("Enter Name\t : ");
		String ename = read.nextLine();
		
		System.out.print("Enter Salary\t : ");
		double esal = read.nextDouble();
		read.nextLine();
		
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
		System.out.print("DOB (DD/MM/YYYY)\t : ");
        LocalDate dob = LocalDate.parse(read.nextLine(), dateTimeFormatter);
        
		System.out.print("Hire Date (DD/MM/YYYY)   : ");
		LocalDate hdate = LocalDate.parse(read.nextLine(), dateTimeFormatter);
		
		if(etype == 1) {
			System.out.print("Enter Subject\t : ");
			String esub = read.nextLine();
			System.out.print("Enter Wing\t : ");
			String wing = read.nextLine();
			
			admin.addEmployee(new TeachingEmployee(ename, esal, dob, hdate, esub, wing));
		}else {
			System.out.print("Enter JobRole\t : ");
			String job = read.nextLine();
			
			admin.addEmployee(new NonTeachingEmployee(ename, esal, dob, hdate, job));
		}
		
		System.out.print("Add more Employee ? [Y/N]");
		String choice = read.nextLine();
		return !choice.equalsIgnoreCase("Y");
	}
}
