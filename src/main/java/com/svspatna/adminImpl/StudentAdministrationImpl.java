package com.svspatna.adminImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.InputMismatchException;
import java.util.Scanner;
import com.svspatna.admin.StudentAdministration;
import com.svspatna.bean.Address;
import com.svspatna.bean.Student;
import com.svspatna.bean.Student.Fee;
import com.svspatna.factory.SchoolFactory;
import com.svspatna.service.student.StudentService;
import com.svspatna.service.studentImpl.StudentServiceImpl;

public class StudentAdministrationImpl implements StudentAdministration {
	static Scanner read = new Scanner(System.in);
	static StudentService admin = new StudentServiceImpl();

	public void mainMenu() {
		boolean quit = false;
		while (!quit) {
			System.out.println("\n====================================");
			System.out.println("1. New Addmission\n" + 
							   "2. Remove Student\n" + 
							   "3. Show Student Details\n" + 
							   "4. Fee Payment\n" + 
							   "5. Marks Entry\n" + 
							   "6. Show Progress Report\n" + 
							   "7. Promote Student\n" +
							   "8. Show Payment History\n" +
							   "9. Exit");
			System.out.println("------------------------------------");
			int option = read.nextInt();
			read.nextLine();
			switch (option) {
			case 1:
				admission(admin);
				break;
			case 2:
				removeStudent(admin);
				break;
			case 3:
				showStudent(admin);
				break;
			case 4:
				feePayment(admin);
				break;
			case 5:
				marksEntry(admin);
				break;
			case 6:
				showProgressReport(admin);
				break;
			case 7:
				promoteStudent(admin);
				break;
			case 8:
				paymentHistory(admin);
				break;
			case 9:
				quit = true;
				break;
			default:
				System.out.println("Invalid option");
			}
		}

	}

	private void paymentHistory(StudentService admin) {
		System.out.print("Enter Registration Number : ");
		admin.printPaymentHistory(read.nextLine());
	}

	private static void feePayment(StudentService admin) {
		System.out.print("Enter Registration Number : ");
		int i = admin.feePayment(read.nextLine());
		if(i>0) {
			System.out.println("Fee Paid");
		}
	}

	private static void showStudent(StudentService admin) {
		System.out.print("Enter Registration Number : ");
		admin.showStudentDetails(read.nextLine());
	}

	private static void removeStudent(StudentService admin) {
		System.out.print("Enter Registration Number : ");
		int i = admin.removeStudent(read.nextLine());
		if(i>0)
			System.out.println("Student Record Deleted");
		else
			System.out.println("Student Record Not Deleted");
	}

	private static void marksEntry(StudentService admin) {
		boolean quit = false;
		while (!quit) {
			System.out.print("Enter Registration Number : ");
			admin.marksEntry(read.nextLine());
			System.out.print("Another Student Marks Entry ? [Y/N] ");
			String ch = read.nextLine();
			if (ch.equalsIgnoreCase("N")) {
				quit = true;
			}
		}
	}

	private static void showProgressReport(StudentService admin) {
		System.out.print("Enter Registration Number : ");
		admin.printProgressReport(read.nextLine());
	}
	
	private static void promoteStudent(StudentService admin) {
		System.out.print("Enter Registration Number :");
		admin.promoteClass(read.nextLine());
	}

	private static void admission(StudentService admin) {
		Student s = (Student)SchoolFactory.getInstance("Student");
		Student.Fee f = (Fee) SchoolFactory.getInstance("Fee");
		System.out.println("-----------ADMISSION FORM-----------");
		try {

			System.out.print("Enter Student Name\t: ");
			s.setName(read.nextLine());

			System.out.print("Enter class section\t: ");
			s.setClassSection(read.nextLine());

			System.out.print("Enter Father's Name\t: ");
			s.setFatherName(read.nextLine());
			
			System.out.print("Enter Contact No.\t: ");
			s.setContact(read.nextLine());
			
			System.out.print("Enter Mother's Name\t: ");
			s.setMotherName(read.nextLine());
		
			System.out.print("Enter Date of Birth\t: ");
			try {
				s.setDob(new SimpleDateFormat("DD-mm-yyyy").parse(read.nextLine()));
			} catch (ParseException e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println("Enter Address");
			Address saddr = (Address)SchoolFactory.getInstance("Address");
			System.out.print("\tState\t\t: ");
			saddr.setState(read.nextLine());
			System.out.print("\tCity\t\t: ");
			saddr.setCity(read.nextLine());
			System.out.print("\tArea\t\t: ");
			saddr.setArea(read.nextLine());
			System.out.print("\tZip Code\t: ");
			saddr.setZipCode(read.nextInt());
			read.nextLine();
			System.out.print("\tHouse No\t: ");
			saddr.setHouseNumber(read.nextLine());
			s.setAddress(saddr);

			System.out.print("School Transport [Y/N]\t: ");
			String transport = read.nextLine();
			if (transport.equalsIgnoreCase("Y"))
				s.setSchoolTransport(true);
			else
				s.setSchoolTransport(false);

			System.out.print("Enter Admission Fee\t: ");
			f.setAddmissionFee(read.nextDouble());

			System.out.print("Enter Monthly Fee\t: ");
			f.setMonthlyFee(read.nextDouble());

			System.out.print("Enter Uniform Cost\t: ");
			f.setUniformCost(read.nextDouble());

			System.out.print("Enter Book Cost\t\t: ");
			f.setBooksCost(read.nextDouble());

			System.out.print("Enter Extra Charges\t: ");
			f.setExtracurricularCharges(read.nextDouble());

			if (s.isSchoolTransport()) {
				System.out.print("Enter Transportation Cost : ");
				f.setTransportationCost(read.nextDouble());
			} else
				f.setTransportationCost(0.0);
			read.nextLine();
			
			f.setDues(0.0);
			
			s.setFee(f);
			int i = admin.addStudent(s);
			if (i > 0) {
				System.out.println("\n" + s.getName() + " Added Successfuly");
				System.out.println("School ID : " + s.getId() + "\n");
			}
			
		} catch (InputMismatchException e) {
			System.out.println("Wrong Data Input!\nTry Again..");
			read.nextLine();
		}
	}
}
