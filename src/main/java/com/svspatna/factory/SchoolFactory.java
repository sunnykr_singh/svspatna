package com.svspatna.factory;

import com.svspatna.bean.Address;
import com.svspatna.bean.Student;
import com.svspatna.bean.StudentProgressReport;
import com.svspatna.daoImpl.StudentDaoImpl;
import com.svspatna.paymentImpl.PaymentModeImpl;

public class SchoolFactory {
	public static Object getInstance(String className) {
		
		if(className.equals("Student"))
			return new Student();
		else if(className.equals("StudentDao"))
			return new StudentDaoImpl();
		else if(className.equals("Fee"))
			return new Student().new Fee();
		else if(className.equals("StudentProgressReport"))
			return new StudentProgressReport();
		else if(className.equals("PaymentMode"))
			return new PaymentModeImpl();
		else if(className.equals("Address"))
			return new Address();
		return null;
	}
}
