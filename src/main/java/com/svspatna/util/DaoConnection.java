package com.svspatna.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DaoConnection {
	public static Connection getConnection() {
		String url = "jdbc:postgresql://localhost:5432/svspatna";
		String usr = "postgres";
		String pwd = "postgres";
		try {
			Class.forName("org.postgresql.Driver");
			return DriverManager.getConnection(url,usr,pwd);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return null;
	}
}
