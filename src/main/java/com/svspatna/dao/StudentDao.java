package com.svspatna.dao;

import java.util.ArrayList;

import com.svspatna.bean.Student;
import com.svspatna.bean.Student.Fee;
import com.svspatna.bean.StudentProgressReport;

public interface StudentDao {
	
	int addStudent(Student sbean);
	int removeStudent(String id);
	Student getStudentData(String id);
	
	int feePayment(Student.Fee fee, String id);
	ArrayList<Fee> getPaymentHistory(String id);
	Fee getFeeDetails(String id);
	Fee getMonthlyPaybleAmmount(String id);
	
	int insertProgressReport(StudentProgressReport preport, String id);
	int promoteStudent(String id, String newClassSection);
	ArrayList<StudentProgressReport> getStudentProgressReport(String id);
}
