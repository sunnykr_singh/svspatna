package com.svspatna.dao;

import com.svspatna.bean.Employee;

public interface EmployeeDao {
	Employee getEmployee(String ecode);
}
